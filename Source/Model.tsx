import * as React from "react";
import * as ReactDOM from "react-dom";
import { ListItem, IListItemProps } from "./ListItem";
import { Goal } from "./Goal";

interface IModelState
{
  NextId?: number;
  Goals?: Goal[];
  NewGoalDescription?: string;
}

export class Model extends React.Component<any, IModelState>
{
  constructor()
  {
    super();
    this.state = { NextId: 1, Goals: [], NewGoalDescription: ""}

    this.HandleChange = this.HandleChange.bind(this);
    this.HandleAddClick = this.HandleAddClick.bind(this);
    this.RemoveItem = this.RemoveItem.bind(this);
  }

  private HandleAddClick(): void
  {
    let goal = new Goal(this.state.NewGoalDescription, false);
    goal.Id = this.state.NextId++;
    this.state.Goals.push(goal);
    this.state.NewGoalDescription = "";

    this.setState(this.state);
  }

  private HandleChange(e: any): void
  {
    let input: HTMLInputElement = e.target;
    this.setState({ NewGoalDescription: input.value});
  }

  private RemoveItem(id: number): void
  {
    let goals = this.state.Goals.filter(i => i.Id !== id);
    this.setState({ Goals: goals});
  }

  public render() 
  {
    let goals = this.state.Goals.map(goal => {
            return <ListItem key={ goal.Id } Goal={goal} onDelete={this.RemoveItem}/>;
        }); 
    return <div> 
      <table>
        <thead>
          <tr>
            <th>Id</th>
            <th>Description</th>    
            <th>Completed</th>
          </tr>
        </thead>
        <tbody>
          {goals}
        </tbody>
      </table>
      <div>
        <input
          type="text"
          value={this.state.NewGoalDescription}
          onChange={this.HandleChange}
        />
        <button onClick={this.HandleAddClick}> 
          Add 
        </button>
      </div>
    </div>;
  } 
}