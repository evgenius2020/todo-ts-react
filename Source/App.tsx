/// <reference path="../typings/tsd.d.ts" />
import * as React from "react";
import * as ReactDOM from "react-dom";
import { Model } from "./Model";

ReactDOM.render (
  React.createElement(Model), 
  document.getElementById('Application')
);