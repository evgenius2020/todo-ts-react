import * as React from "react";
import * as ReactDOM from "react-dom";
import { Goal } from "./Goal";

export interface IListItemProps 
{
  Goal: Goal;
  onDelete: (id: number) => void;
}

export interface IListItemState
{
  IsEditing?: boolean;
  NewDescription?: string;
}

export class ListItem extends React.Component<IListItemProps, IListItemState>
{
  constructor()
  {
    super();
    this.state = {IsEditing: false};

    this.ChangeStatus = this.ChangeStatus.bind(this);
    this.ChangeEditMode = this.ChangeEditMode.bind(this);
    this.HandleChange = this.HandleChange.bind(this);        
    this.RemoveItem = this.RemoveItem.bind(this);
    this.render = this.render.bind(this);
  }

  private ChangeStatus(): void
  {
    this.props.Goal.Completed = !this.props.Goal.Completed;
    this.setState(this.state);
  }

  private ChangeEditMode(): void
  {
    if (this.state.IsEditing)
    {
      this.props.Goal.Description = this.state.NewDescription;
    }
    this.setState({IsEditing: !this.state.IsEditing, NewDescription: this.props.Goal.Description});
  }

  private HandleChange(e: any): void
  {
    let input: HTMLInputElement = e.target;
    this.setState({NewDescription: input.value});
  }

  private RemoveItem(): void
  {
    this.props.onDelete(this.props.Goal.Id);
  }

  public render() 
  {
    let descriptionField = 
      this.state.IsEditing ? 
        <input value={this.state.NewDescription} onChange={this.HandleChange} /> :
        <span > {this.props.Goal.Description} </span>;
    return <tr>
          <th>{this.props.Goal.Id}</th>
          <th onDoubleClick={this.ChangeEditMode}>
            {descriptionField}
          </th>
          <th>
            <input type="checkbox" checked={this.props.Goal.Completed} onChange={this.ChangeStatus}/>
          </th>
          <th>
            <button onClick={this.RemoveItem}>
              [X]
            </button>
          </th>
        </tr>
  }
}