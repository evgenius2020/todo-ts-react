export class Goal
{
  Id: number;
  Description: string;
  Completed: boolean;

  constructor (description: string = "", completed: boolean = true)
  {
    this.Id = 0;
    this.Description = description;
    this.Completed = completed;
  }
}